# Custom-Elive-Icon-Set

See the "sample.png" to see what they look like.

![Sample](https://gitlab.com/triantares/custom-elive-icon-set/-/raw/master/Sample.png)


Custom system action icons loosely based on the Breeze icon set.
Initially for my own E16 desktop usage but feel free to download and use.



